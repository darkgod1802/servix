<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHSolicitudRegistros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('h_solicitudRegistros', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->boolean('state');
          $table->Integer('id_trabajadores');
          $table->Integer('id_responsableSistema');
          $table->timestamps();
          $table->softDeletes();
          $table->integer('tx_usuario_id');
          $table->string('tx_host');
          $table->integer('id_x');
          $table->bigInteger('trabajadores_id')->unsigned();
          $table->foreign('trabajadores_id')->references('id')->on('trabajadores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('h_solicitudRegistros');
    }
}
