<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudRegistrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudRegistros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('state');
            $table->bigInteger('trabajadores_id')->unsigned();
            $table->foreign('trabajadores_id')->references('id')->on('trabajadores');
            $table->bigInteger('responsableSistema_id')->unsigned();
            $table->foreign('responsableSistema_id')->references('id')->on('responsableSistema');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('tx_usuario_id');
            $table->string('tx_host');
            $table->integer('id_x');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudRegistros');
    }
}
