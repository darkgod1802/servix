<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoOpinionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipoOpiniones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('state');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /****
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipOpiniones');
    }
}
