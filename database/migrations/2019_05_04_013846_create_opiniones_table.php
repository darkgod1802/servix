<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpinionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('opiniones', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('comentario');
                $table->bigInteger('tipoOpiniones_id')->unsigned();
                $table->foreign('tipoOpiniones_id')->references('id')->on('tipoOpiniones');
                $table->timestamps();
                $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opiniones');
    }
}
