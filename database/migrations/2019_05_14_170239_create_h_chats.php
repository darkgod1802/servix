<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHChats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('h_chats', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('message');
          $table->Integer('id_clientes');
          $table->Integer('id_trabajadores');
          $table->timestamps();
          $table->softDeletes();
          $table->bigInteger('clientes_id')->unsigned();
          $table->foreign('clientes_id')->references('id')->on('clientes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('h_chats');
    }
}
