<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->bigInteger('trabCats_id')->unsigned();
            $table->foreign('trabCats_id')->references('id')->on('trabCats');
            $table->bigInteger('opiniones_id')->unsigned();
            $table->foreign('opiniones_id')->references('id')->on('opiniones');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('tx_usuario_id');
            $table->string('tx_host');
            $table->integer('id_x');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publicaciones');
    }
}
