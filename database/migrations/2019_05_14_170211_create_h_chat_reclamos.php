<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHChatReclamos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('h_chatReclamos', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('message');
          $table->Integer('id_publicaciones');
          $table->Integer('id_responsableSistema');
          $table->timestamps();
          $table->softDeletes();
          $table->integer('tx_usuario_id');
          $table->string('tx_host');
          $table->integer('id_x');
          $table->bigInteger('publicaciones_id')->unsigned();
          $table->foreign('publicaciones_id')->references('id')->on('publicaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('h_chatReclamos');
    }
}
