<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudContratos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('price');
            $table->integer('cant_days');
            $table->string('state');
            $table->bigInteger('publicaciones_id')->unsigned();
            $table->foreign('publicaciones_id')->references('id')->on('publicaciones');
            $table->bigInteger('clientes_id')->unsigned();
            $table->foreign('clientes_id')->references('id')->on('clientes');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('tx_usuario_id');
            $table->string('tx_host');
            $table->integer('id_x');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudContratos');
    }
}
