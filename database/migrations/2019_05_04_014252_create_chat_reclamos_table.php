<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatReclamosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatReclamos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('message');
            $table->bigInteger('publicaciones_id')->unsigned();
            $table->foreign('publicaciones_id')->references('id')->on('publicaciones');
            $table->bigInteger('responsableSistema_id')->unsigned();
            $table->foreign('responsableSistema_id')->references('id')->on('responsableSistema');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('tx_usuario_id');
            $table->string('tx_host');
            $table->integer('id_x');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatReclamos');
    }
}
