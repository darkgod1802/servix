<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHSolicitudContratos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('h_solicitudContratos', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->double('price');
          $table->integer('cant_days');
          $table->string('state');
          $table->Integer('id_publicaciones');
          $table->Integer('id_clientes');
          $table->timestamps();
          $table->softDeletes();
          $table->integer('tx_usuario_id');
          $table->string('tx_host');
          $table->integer('id_x');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('h_solicitudContratos');
    }
}
