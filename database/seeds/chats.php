<?php

use Illuminate\Database\Seeder;

class chats extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0 ; $i <= 10 ; $i++){
	    	DB::table('chats')->insert(array(
	    		'mensaje' => 'Buen dia soy'.$i.'y quisiera mas informacion sobre las tutorias',
	    		'fecha' => date('Y-m-d H:m:s'),
	    		'clientes_id' => 1,
	    		'trabajadores_id' => 1,
	    		'created_at' => date('Y-m-d H:m:s'),
           		'updated_at' => date('Y-m-d H:m:s')
	    	));
        }

        $this->command->info('tabla rellenada correctamente');

    }
}
