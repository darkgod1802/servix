<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class personas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0 ; $i <= 30 ; $i++){
	    	DB::table('personas')->insert(array(
                'ci'     => (String)$faker->numberBetween(1000000, 9999999),
                'surname' => $faker->lastName,
                'name' => $faker->firstName,
	    		'address' => "{$faker->city}, {$faker->state}",
	    		'fono' =>(String)$faker->numberBetween(60000000, 79999999),
                'created_at' => $faker->dateTime($max = 'now'),
	    	));
        }

        $this->command->info('tabla rellenada correctamente');
    }
}
