<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class clientes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0 ; $i <= 10 ; $i++){
	    	DB::table('clientes')->insert(array(
	    		'personas_id' => ($i+1),
                'email' => $faker->email,
	    		'password'     => $faker->password,
	    		/**
	    		* Hash
	    		*'password' =>Hash::make('password'),
	    		*
	    		*/
	    		'characteristics' => $faker->sentence(10),
                'created_at' => date('Y-m-d H:m:s'),
           		'updated_at' => date('Y-m-d H:m:s'),
	    	));
        }

        $this->command->info('tabla rellenada correctamente');
    }
}
