<?php

use Illuminate\Database\Seeder;

class responsableSistema extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0 ; $i <= 1 ; $i++){
	    	DB::table('responsableSistema')->insert(array(
	    		'nombre' => 'Responsable'.$i,
	    		'ci' => '321654'.$i,
	    		'direccion' => 'El alto # '.$i,
	    		'correo' => 'responsable'.$i.'@gmail.com',
	    		'contraseña' => 'contrase'.$i.'ña',
	    		'celular' => '7326232'.$i,
                'email' => $faker->email,
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s')
	    	));
        }

        $this->command->info('tabla rellenada correctamente');
    }
}
