<?php

use Illuminate\Database\Seeder;

class tipoOpiniones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0 ; $i <= 1 ; $i++){
	    	DB::table('tipoOpiniones')->insert(array(
	    		'state' => 0,
                'created_at' => date('Y-m-d H:m:s'),
	    	));
        }

        $this->command->info('tabla rellenada correctamente');
    }
}
