<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(personas::class);
        $this->call(trabajadores::class);
        $this->call(clientes::class);
        $this->call(categorias::class);
        $this->call(tipoOpiniones::class);
        $this->call(opiniones::class);
        $this->call(trabCats::class);
        $this->call(publicaciones::class);   
    }
}
