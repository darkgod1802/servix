<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class publicaciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0 ; $i <= 10 ; $i++){
	    	DB::table('publicaciones')->insert(array(
	    		'description' => "publicacion {$i} \n {$faker->sentence(20)}",
	    		'trabCats_id' => 5,
	    		'opiniones_id' => 5,
	    		'created_at' => date('Y-m-d H:m:s'),
           		'updated_at' => date('Y-m-d H:m:s'),
                'tx_usuario_id' => 1,
                'tx_host' => $faker->sentence(2),
                'id_x' => 1
            ));
        }

        $this->command->info('tabla rellenada correctamente');
    }
}
