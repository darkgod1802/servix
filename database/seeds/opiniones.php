<?php

use Illuminate\Database\Seeder;

class opiniones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0 ; $i <= 10 ; $i++){
	    	DB::table('opiniones')->insert(array(
	    		'comentario' => 'el servicio fue bueno'.$i,
	    		'tipoOpiniones_id' => 1,
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s')
	    	));
        }

        $this->command->info('tabla rellenada correctamente');
    }
}
