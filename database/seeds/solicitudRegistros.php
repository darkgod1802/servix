<?php

use Illuminate\Database\Seeder;

class solicitudRegistros extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0 ; $i <= 10 ; $i++){
	    	DB::table('solicitudRegistros')->insert(array(
	    		'fecha' => date('Y-m-d H:m:s'),
	    		'estado' => 0,
	    		'trabajadores_id' => 1,
	    		'responsableSistema_id' => 1,
	    		'created_at' => date('Y-m-d H:m:s'),
           		'updated_at' => date('Y-m-d H:m:s')
	    	));
        }

        $this->command->info('tabla rellenada correctamente');
    }
}
