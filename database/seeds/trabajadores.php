<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class trabajadores extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0 ; $i <= 9 ; $i++){
	    	DB::table('trabajadores')->insert(array(
                'personas_id' => ($i+10),
	    		'horario_entrada' => '07:00:00',
                'horario_salida' => '19:00:00',
                'email' => $faker->email,
                'password'     => $faker->password,
                /**
	    		* Hash
	    		*'password' =>Hash::make('password'),
	    		*
	    		*/
                'created_at' => date('Y-m-d H:m:s'),
           		'updated_at' => date('Y-m-d H:m:s'),
	    	));
        }

        $this->command->info('tabla rellenada correctamente');
    }
}
