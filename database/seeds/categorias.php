<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class categorias extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0 ; $i <= 10 ; $i++){
	    	DB::table('categorias')->insert(array(
	    		'name' => 'Categoria '.$i,
	    		'description' => 'la categoria'.$i.'es el arte de la representación gráfica utilizando pigmentos mezclados',
	    		'image' => 'imagen'.$i,
                'created_at' => $faker->dateTime($max = 'now')
	    	));
        }

        $this->command->info('tabla rellenada correctamente');
    }
}
