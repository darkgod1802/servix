<?php

use Illuminate\Database\Seeder;

class chatReclamos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0 ; $i <= 10 ; $i++){
	    	DB::table('chatReclamos')->insert(array(
	    		'mensaje' => 'Buen dia trabajador'.$i.'lo requerimos en la oficina ',
	    		'fecha' => date('Y-m-d H:m:s'),
	    		'publicaciones_id' => 10,
	    		'responsableSistema_id' => 1,
	    		'created_at' => date('Y-m-d H:m:s'),
           		'updated_at' => date('Y-m-d H:m:s')
	    	));
        }

        $this->command->info('tabla rellenada correctamente');
    }
}
