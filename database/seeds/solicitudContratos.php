<?php

use Illuminate\Database\Seeder;

class solicitudContratos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0 ; $i <= 10 ; $i++){
	    	DB::table('solicitudContratos')->insert(array(
	    		'costo' => '25'.$i,
	    		'fecha' => date('Y-m-d H:m:s'),
	    		'cant_dias' => '1'.$i,
	    		'estado' => 0,
	    		'clientes_id' => 5,
	    		'created_at' => date('Y-m-d H:m:s'),
           		'updated_at' => date('Y-m-d H:m:s')
	    	));
        }

        $this->command->info('tabla rellenada correctamente');
    }
}
