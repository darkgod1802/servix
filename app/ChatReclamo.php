<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatReclamo extends Model
{
    public function responsableSistema()
    {
	return $this->belongsTo('App\Responsable','responsableSistema_id');
    }

    public function publicaciones()
    {
	return $this->belongsTo('App\Publicacion','publicaciones_id');
    }

    public function h_chatReclamos()
    {
	return $this->hasMany('App\H_chatReclamo');
    }
}
