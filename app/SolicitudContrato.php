<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;

class SolicitudContrato extends Model
{
    protected $table='solicitudContratos';
    protected $hidden=['created_at','updated_at','contraseña'];

}
