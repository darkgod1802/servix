<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;

class H_solicitudRegistro extends Model
{
    public function solicitudRegistros()
    {
      return $this->belongsTo('App\SolicitudRegistro','solicitudRegistros_id');
    }
}
