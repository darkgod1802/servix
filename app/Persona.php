<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
  protected $table = 'personas';
  protected $fillable = ['ci','surname','name','address','email','fono','characteristics'];
  protected $hidden =['created_at','updated_at','deleted_at'];
  /*  public function clientes()
    {
      return $this->hasMany('App\Cliente');
    }*/

   /* public function trabajadores()
    {
      return $this->hasMany('App\Trabajador');
    }

    public function responsableSistema()
    {
      return $this->hasMany('App\Responsable');
    }*/
    public function trabajador(){
      return $this->hasOne('servix\Trabajador','personas_id');
    }
    public function cliente(){
      return $this->hasOne('servix\Cliente','personas_id');
    }
    public function responsable(){
      return $this->hasOne('servix\Responsable','personas_id');
    }
}
