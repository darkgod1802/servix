<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;

class Responsable extends Model
{
    
 /*   public function solicitudRegistros()
    {
	     return $this->hasMany('App\SolicitudRegistro');
    }

    public function trabajadores()
    {
	     return $this->hasMany('App\Trabajador');
    }*/
    protected $table = 'responsableSistema';
    protected $hidden = ['password','created_at','updated_at','deleted_at'];
    public function persona(){
	     return $this->belongsTo('servix\Persona','personas_id');
    }
}
