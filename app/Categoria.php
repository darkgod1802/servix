<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';
    protected $primaryKey = 'id';
    protected $hidden =['created_at','updated_at','deleted_at','pivot'];
  /*  public function trabCats()
    {
	return $this->hasMany('App\TrabCat');
    }*/
}
