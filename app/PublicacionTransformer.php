<?php 
namespace servix;

use servix\Publicacion;
use League\Fractal\TransformerAbstract;
class PublicacionTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Publicacion $publicacion)
    {
        return [
            'id'  => (int) $publicacion->id,
            'description' => $publicacion->description,
            'fecha publicacion' => $publicacion->created_at
        ];
    }
}