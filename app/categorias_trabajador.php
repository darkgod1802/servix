<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;

class categorias_trabajador extends Model
{
  protected $table = 'categoria_trabajador';
  protected $primaryKey = 'id';

 /*   protected $table = 'trabCats';
    protected $primaryKey = 'id';
    protected $hidden=['created_at','updated_at'];*/
  /*  
    public function categorias()
    {
	     return $this->belongsTo('App\Categoria','categorias_id');
    }

    public function trabajadores()
    {
	     return $this->belongsTo('App\Trabajador','trabajadores_id');
    }

    public function publicaciones()
    {
	     return $this->hasMany('App\Publicacion');
    }*/
    public function publicaciones(){
      return $this->hasMany('servix\Publicacion','trabCats_id');
    }

}
