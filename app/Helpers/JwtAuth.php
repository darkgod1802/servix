<?php
namespace servix\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use servix\Responsable;
use servix\Trabajador;
use servix\Cliente;


class JwtAuth{
	public $key;

	public function __construct(){
		$this->key = '*esta*es*mi*clave*secreta*9876543210*';
	}

	public function signup($email, $password, $getToken = null){

		$responsable = Responsable::where(
				array(
					'email' => $email,
					'password' => $password
				))->first();
		$cliente = Cliente::where(
				array(
					'email' => $email,
					'password' => $password
				))->first();
		$trabajador = Trabajador::where(
				array(
					'email' => $email,
					'password' => $password
				))->first();

		$signup = false;
		if(is_object($responsable)||is_object($cliente)||is_object($trabajador)){
			$signup = true;
		}

		if($signup){
			if(is_object($responsable)){
				// Generar el token y devolverlo
			$token = array(
				'email' => $responsable->email,
				'password' => $responsable->password,
				'iat' => time(),
				'exp' => time() + (7 * 24 * 60 * 60));
			$jwt = JWT::encode($token, $this->key, 'HS256');
			$decoded = JWT::decode($jwt, $this->key, array('HS256'));

			if(is_null($getToken)){
				return $jwt;
			}else{
				return $decoded;
			}} 
			if(is_object($cliente)){
				$token = array(
				'email' => $cliente->email,
				'password' => $cliente->password,
				'iat' => time(),
				'exp' => time() + (7 * 24 * 60 * 60));
				$jwt = JWT::encode($token, $this->key, 'HS256');
			$decoded = JWT::decode($jwt, $this->key, array('HS256'));

			if(is_null($getToken)){
				return $jwt;
			}else{
				return $decoded;
			}}
			if(is_object($trabajador)){
				$token = array(
				'email' => $trabajador->email,
				'password' => $trabajador->password,
				'iat' => time(),
				'exp' => time() + (7 * 24 * 60 * 60));
				$jwt = JWT::encode($token, $this->key, 'HS256');
			$decoded = JWT::decode($jwt, $this->key, array('HS256'));

			if(is_null($getToken)){
				return $jwt;
			}else{
				return $decoded;
			}}
			else{
				return array('status' => 'error', 'message' => 'Login ha fallado !!');
			}
		}else{

			// Devolver un error
			return array('status' => 'error', 'message' => 'Login ha fallado !!');
		}

	}


	public function checkToken($jwt, $getIdentity = false){
		$auth = false;

		try{
			$decoded = JWT::decode($jwt, $this->key, array('HS256'));

		}catch(\UnexpectedValueException $e){
			$auth = false;
		}catch(\DomainException $e){
			$auth = false;
		}

		if(isset($decoded) && is_object($decoded) && isset($decoded->sub)){
			$auth = true;
		}else{
			$auth = false;
		}

		if($getIdentity){
			return $decoded;
		}

		return $auth;
	}


}