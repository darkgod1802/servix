<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;

class H_solicitudContrato extends Model
{
    public function solicitudContratos()
    {
      return $this->belongsTo('App\SolicitudContrato','solicitudContratos_id');
    }
}
