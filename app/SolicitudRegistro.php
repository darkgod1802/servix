<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudRegistro extends Model
{
    public function responsableSistema()
    {
	return $this->belongsTo('App\Responsable','responsableSistema_id');
    }

    public function trabajadores()
    {
	return $this->belongsTo('App\Trabajador','trabajadores_id');
    }

    public function H_solicitudRegistro()
    {
	return $this->hasMany('App\H_solicitudRegistro');
    }
}
