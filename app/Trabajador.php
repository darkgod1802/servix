<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class Trabajador extends Model
{
    protected $table = 'trabajadores';

    protected $fillable=['horario_entrada','horario_salida','password'];
    protected $hidden =['password','created_at','updated_at','deleted_at'];
    
    public function chats()
    {
	return $this->hasMany('App\Chat');
    }
    public function guardar($datos,$cod){
        $this->horario_entrada=$datos->get('horario_entrada');
        $this->personas_id=$cod;
        $this->horario_salida=$datos->get('horario_salida');
        $this->password=Hash::make($datos->get('password'));
    }

   /* public function trabCats()
    {
	return $this->hasMany('App\TrabCat');
    }*/

    public function solicitudResgistros()
    {
	return $this->hasMany('App\SolicitudResgistro');
    }

    public function persona(){
	    return $this->belongsTo('servix\Persona','personas_id');
    }
    public function categorias(){
        return $this->belongsToMany('servix\Categoria');
    }

}
