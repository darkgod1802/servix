<?php 
namespace servix;

use servix\Trabajador;
use servix\Persona;
use League\Fractal\TransformerAbstract;
class TrabajadorTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Trabajador $trabajador)
    {
        $persona = Persona::find($trabajador->personas_id);
        if (! $persona){
            return $this->errorNotFound(
                'Persona no encontrada'
            ); 
        }
        return [
            'id'  => (int) $trabajador->id,
            'nombre'            => $persona->name,
            'apellido'          => $persona->surname,
            'direccion'         => $persona->address,
            'email'             => $persona->email,
            'fono'              => $persona->fono,
	    	'horario_entrada' => $trabajador->horario_entrada,
	    	'horario_salida' => $trabajador->horario_salida,
        ];
    }
}