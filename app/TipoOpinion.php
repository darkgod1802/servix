<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;

class TipoOpinion extends Model
{
	protected $table='tipoOpiniones';
    
    protected $hidden = ['created_at','updated_at','deleted_at'];

    public function opiniones()
    {
	return $this->hasMany('App\Opinion');
    }
}
