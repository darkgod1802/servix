<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;

class H_chat extends Model
{
    public function chats()
    {
      return $this->belongsTo('App\Chat','chat_id');
    }
}
