<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;

class H_chatReclamo extends Model
{
    public function chatReclamos()
    {
      return $this->belongsTo('App\ChatReclamo','chatReclamos_id');
    }
}
