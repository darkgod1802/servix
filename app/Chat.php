<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    public function clientes()
    {
	return $this->belongsTo('App\Cliente','clientes_id');
    }

    public function trabajadores()
    {
	return $this->belongsTo('App\Trabajador','trabajadores_id');
    }

    public function chats()
    {
	return $this->hasMany('App\H_chat');
    }
}
