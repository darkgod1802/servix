<?php

namespace servix;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    
    protected $table = 'clientes';
    protected $hidden=['created_at','updated_at','contraseña'];
    public function chats()
    {
    return $this->hasMany('servix\Chat');
    }

    public function solicitudContratos()
    {
    return $this->hasMany('servix\SolicitudContrato','clientes_id');
    }

    public function persona()
    {
    return $this->belongsTo('servix\Persona','personas_id');
    }
}
