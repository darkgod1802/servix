<?php

namespace servix;

use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
    public function publicaciones()
    {
	return $this->hasMany('App\Publicacion');
    }

    public function tipoOpinion(){
        return $this->hasMany('servix\TipoOpinion','tipoOpiniones_id');
    }
 /*   public function tipoOpiniones()
    {
	return $this->belongsTo('App\TipoOpinion','tipoOpiniones_id');
    }*/
}
