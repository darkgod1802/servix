<?php

namespace servix\Http\Controllers;

use servix\Cliente;
use servix\ClienteTransformer;
use Illuminate\Http\Request;

use servix\solicitudContrato;
use servix\Publicacion;

use servix\Http\Controllers\T_Persona;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class ClienteController extends ApiController
{
    use T_Persona;
    public function createCliente(Request $request){
        $p=$this->createPersona($request)->id;
        $cliente=new Cliente;
        $cliente->password=Hash::make($request->get('password'));
        $cliente.save();
    }
    public function updateCliente($id,Request $request){
        $this->updatePersona(Cliente::find($id)->persona->id,$request);
        
    }
    public function deleteCliente(Request $request){
    	echo "delete cliente"; die();
    }
    /**
    * @OA\Get(
    *     path="/v1/api/clients/{id}",
    *     summary="Mostrar un usuario en función a su id",
    *     tags={"Clientes"},
    *     description="Retorna un cliente identificandolo por su id.",
     *    operationId="readCliente",
     *    @OA\Parameter(
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Operación exitosa",
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="El id ingresado es invalido"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Cliente no encontrado"
     *     ),
     * )
    */
    public function readCliente($id){
        $token=JWTAuth::parseToken();
        $usuario_id_token=$token->getPayload()->get('sub');
        $rol_token=$token->getPayload()->get('rol');
        Cliente::take(10)->get();
       // return $this->readClientePersona($id);
        if($id!=$usuario_id_token){
            return response()->json(['error'=>'Token no valido para ver este cliente']);
        }
        $dato2=Cliente::find($id);
        $dato1=$dato2->persona;
        //MOVER A UN MODELO
        $dato3=[
            'id'=>$dato2->id,
            'persona_id'=>$dato1->id,
            'ci'=>$dato1->ci,
            'surname'=>$dato1->surname,
            'name'=>$dato1->name,
            'address'=>$dato1->address,
            'email'=>$dato2->email,
            'fono'=>$dato1->fono,
            'characteristics'=>$dato2->characteristics
        ];
       return json_encode($dato3);
    }
            
    /**
    * @OA\Get(
    *     path="/v1/api/clients/",
    *     summary="Listar clientes",
    *     tags={"Clientes"},
    *     description="Retorna una pila de 10 clientes",
     *    operationId="listCliente",
     *     @OA\Response(
     *         response=200,
     *         description="Operación exitosa",
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Valor invalido"
     *     ),
     * )
    */
    public function listCliente(Request $request){
        $token=JWTAuth::parseToken();

        $rol_token=$token->getPayload()->get('rol');
        if($rol_token!='administrador'){
            return response()->json(['error'=>'Token no valido para esta accion']);
        }
        Cliente::take(10)->get();
        $clientes = [];
        foreach (Cliente::all() as $cliente){
            $persona=$cliente->persona;
            $dato=[
                'id'=>$cliente->id,
                'persona_id'=>$persona->id,
                'ci'=>$persona->ci,
                'surname'=>$persona->surname,
                'name'=>$persona->name,
                'address'=>$persona->address,
                'email'=>$cliente->email,
                'fono'=>$persona->fono,
                'characteristics'=>$cliente->characteristics
            ];
            $clientes[]=$dato;
        }
        return json_encode($clientes);
    }
    public function imageCliente(Request $request){
    	echo "imagen cliente"; die();
    }
    public function solicitudCliente($id){
        $solicitudes = Cliente::find($id)->solicitudContratos;
        echo "MIS SOLICITUDES\n".$solicitudes."\n";
        foreach ($solicitudes as $solicitud){
            $publicacion = Publicacion::find($solicitud->publicaciones_id);
            echo "\nMI PUBLICACION ID ".$solicitud->publicaciones_id."\n";
            echo "\n".$publicacion."\n";
        }
    }
}
