<?php

namespace servix\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use servix\Cliente;
use servix\Persona;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;

class ClienteAuthenticateController extends Controller
{
    public function authenticate(Request $request){
        $datos=$request->json()->all();
        $cliente=Cliente::where('email', '=', $datos['email'])->first();
        if($cliente!=null){
            $clave1=$cliente->password;
            $clave2=hash("sha256",$datos['password']);
            if($clave1==$clave2){
                //echo "Usuario verificado"."\n";
                $extras = ['name' => $cliente->persona->name, 'rol' => 'cliente'];
                $token=JWTAuth::fromUser($cliente, $extras);
                //echo $token;
                return response()->json(compact('token'));
            }
            else{
                echo "Contraseña incorrecta";
            }

        }else{
            return response()->json(['error'=>'Cliente inexistente']);
        }



//        $cliente=Cliente::find(1);
//        $token = JWTAuth::fromUser($cliente);
//        echo $token;
//        $cliente = Cliente::find(5);
//        $persona=$cliente->persona;
//        $extras = ['foo' => 'bar', 'baz' => 'bob'];
//        $token=JWTAuth::fromUser($cliente, $extras);
//        echo $token;
//        echo $persona->name;


//        $credentials = $request->only('email','password');
//        try{
//            if(!$token=JWTAuth::attemp($credentials)){
//                return response()->json(['error'=>'credencial invalido'],401);
//            }
//
//        }catch(JWTException $e){
//            return response()->json(['error'=>'no se pudo crear el token'],500);
//        }
//        $response=compact('token');
//        $response['trabajador']=Auth::trabajador();
//        return $response;
    }
}
