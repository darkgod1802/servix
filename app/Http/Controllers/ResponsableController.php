<?php

namespace servix\Http\Controllers;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use servix\Helpers\JwtAuth;
use servix\Responsable;
use Validator;

class ResponsableController extends ApiController
{
    public function createResponsable(Request $request){
        $params = $request->all();

        $validor = Validator::make($params, [
            'personas_id' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        if(!is_null($request->personas_id) && !is_null($request->email)&& !is_null($request->password)){

            $responsable = new Responsable();
            $responsable->personas_id = $request->personas_id;
            $responsable->email = $request->email;
            $pwd = hash('sha256', $request->password);       
            $responsable->password = $pwd;
            
            $isset_responsable = Responsable::where('email', '=', $request->email)->count();
            if($isset_responsable == 0){
            $responsable->save();

                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Responsable registrado correctamente!!'
                );
        }else{
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Responsable duplicado, no puede registrarse'
                );
            }
        }
        else{
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Responsable no creada'
            );
        }

        return response()->json($data, 200);
    }
   
    public function readResponsable($id){
        return $this->readResponsablePersona($id);
    }
    public function updateResponsable($id,Request $request){
        $this->updatePersona(Responsable::find($id)->persona->id,$request);
        //AQUI
        $responsable=Responsable::find($id);

        $responsable->save();
        
    }
    public function deleteResponsable(Request $request){
    	echo "borrar responsable";die();
    }
    public function listResponsable(Request $request){
    	echo "list responsable";die();
    }
    public function login(Request $request){
        $jwtAuth = new JwtAuth();
        //Recibir POST
        $params = $request->all();
        
        $validator = Validator::make($params, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $email = $request->email;
        $password = $request->password;
        $getToken = $request->gettoken;
    
        //Cifrar la password
        $pwd = hash('sha256', $request->password);

        if(!is_null($email) && !is_null($password) && ($getToken == null || $getToken == 'false')){
            $signup = $jwtAuth->signup($email, $pwd);

        }elseif($getToken != null){
            //var_dump($getToken); die();
            $signup = $jwtAuth->signup($email, $pwd, $getToken);

        }else{
            $signup = array(
                    'status' => 'error',
                    'message' => 'Envia tus datos por post'
                );
        }

        return response()->json($signup, 200);
    }
}
