<?php

namespace servix\Http\Controllers;

use servix\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends ApiController
{
    public function createCategoria(Request $request){
       // $this->validate($request,['name'=>'required','description'=>'required','image'=>'required']);
    //    Categoria::create($request->all());
     //   return response() ->json(['mensaje'=>'La categoria se creo']);
        $categoria=new Categoria;
        $categoria->name=$request->get('name');
        $categoria->description=$request->get('description');
        $categoria->image=$request->get('image');
        $categoria->save();
     /*   $categoria->name="nombre prueba";
        $categoria->description="descripcion prueba";
        $categoria->image="imagen prueba";
        $categoria->save();*/
    }
    public function listCategoria(){
        $categoria = Categoria::take(10)->get();
        return $categoria;
    }
    public function deleteCategoria($id){
        echo "delete categoria";
        $categoria=Categoria::find($id);
        $categoria->delete();
    }
    public function imageCategoria(Request $request){
    	echo "imagen categoria";
    }
    public function readCategoria($id){
        $categoria = Categoria::find($id);
        return $categoria;
    }
    public function updateCategoria($id,Request $request){
        $metodo=$request->method();
        $categoria=Categoria::find($id);
      //  if($metodo==="PATCH"){
            $name=$request->get('name');
            if($name!=null && $name!=''){
                $categoria->name=$name;
            }
            $description=$request->get('description');
            if($description!=null && $description!=''){
                $categoria->description=$description;
            }
            $image=$request->get('image');
            if($image!=null && $image!=''){
                $categoria->image=$image;
            }
            $categoria->save();
            return "registro editando con patch";
       // }
        //$categoria->save();
        return "estoy en put";
    }

}
