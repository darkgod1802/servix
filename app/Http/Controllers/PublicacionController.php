<?php

namespace servix\Http\Controllers;

use Illuminate\Http\Request;
use servix\Publicacion;
use servix\PublicacionTransformer;

class PublicacionController extends ApiController
{
            /**
    * @OA\Get(
    *     path="/v1/api/publications/{id}",
    *     summary="Mostrar una publicación en función a su id",
    *     tags={"Publicaciones"},
    *     description="Retorna un cliente identificandolo por su id.",
     *    operationId="readPublicacion",
     *    @OA\Parameter(
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Operación exitosa",
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="El id ingresado es invalido"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Publicación no encontrada"
     *     ),
     * )
    */
    public function createPublicacion(Request $request){
        /*  $validatedData = $this->validate($request, [
              'description' => 'required',
              'trabCats_id' => 'required',
          ]);*/
          $data = $request->json()->all();
  
        // echo ($data['description']);
          $trabcat=new TrabCat;
          $trabcat->trabajadores_id=$data['trabajadores_id'];
          $trabcat->categorias_id=$data['categorias_id'];
          $trabcat->save();
  
          $publicacion=new Publicacion();
          $publicacion->description=$data['description'];
          $publicacion->trabCats_id=$trabcat->trabajadores_id;
          $publicacion->save();
          echo "publicacion guardada";
      }
    public function readPublicacion($id){
        $publicacion = Publicacion::find($id);
    }
    public function updatePublicacion(Request $request){
    	echo "actualizar publicacion"; die();
    }
    public function deletePublicacion(Request $request){
    	echo "borrar publicacion"; die();
    }
        /**
    * @OA\Get(
    *     path="/v1/api/publications/",
    *     summary="Listar publicaciones",
    *     tags={"Publicaciones"},
    *     description="Retorna una pila de 10 publicaciones",
     *    operationId="listPublicacion",
     *     @OA\Response(
     *         response=200,
     *         description="Operación exitosa",
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Valor inválido"
     *     ),
     * )
    */
    public function listPublicacion(){
    	return Publicacion::take(10)->get();
    }
}
