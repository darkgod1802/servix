<?php

namespace servix\Http\Controllers;

use Validator;
use servix\Trabajador;
use servix\TrabajadorTransformer;
use servix\categorias_trabajador;
use Illuminate\Http\Request;
use servix\Http\Controllers\T_Persona;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use servix\Helpers\JwtAuth;
use Firebase\JWT\JWT;
class TrabajadorController extends ApiController
{ 
     
    
/**
     * @OA\Post(
     *     path="/api/v1/workers",
     *     tags={"Trabajadores"},
     *     operationId="createTrabajador",
     *     summary="Crear un trabajador",
     *     description="Crea un trabajador.",
     *      @OA\RequestBody(
     *       required=true,
     *       description="Trabajador Creado",
     *   ),
     *   @OA\Response(response="default", description="successful operation")
     * )
     */

    public function createTrabajador(Request $request){
      //  $p=$this->createPersona($request)->id;
     //   $trabajador=new Trabajador;
    //    $trabajador->guardar($request,$p);
       /* echo 'password: '.$k;
        if (Hash::check('b', $k)) {
            echo '\n son iguales';
        }else{
            echo '\n no son iguales';
        }*/
  //      $trabajador->save();
  
  //  $this->validate($request,['nombre'=>'required','ci'=>'required','contraseña'=>'required','celular'=>'required','estado'=>'required']);
        
     $validatedData = $this->validate($request, [
        'ci' => 'required',
        'name' => 'required',
        'fono' => 'required',
    ]);
        $data =$request ->json()->all();
        

        $persona= new Persona;
        $persona->name = $data['name'];
        $persona->ci = $data['ci'];
        $persona->surname = $data['surname'];
        $persona->address = $data['address'];
        $persona->fono = $data['fono'];
        $persona->save();
      //  $persona1= new Persona1;

      //  $idPersona = DB::select('select * from personas where ci = ' ,$data['ci'],);
        echo ($persona->id);
        $trabajador= new Trabajador;
        $trabajador->personas_id=$persona->id;
        $trabajador->horario_entrada=$data['horario_entrada'];
        $trabajador->horario_salida=$data['horario_salida'];
        $trabajador->password=Hash::make($request->get('password'));
        $trabajador->save();
        /* echo 'password: '.$k;
        if (Hash::check('b', $k)) {
            echo '\n son iguales';
        }else{
            echo '\n no son iguales';
        }*/



        echo " persona creada"; 
  
  
  
    }
 /**
    * @OA\Get(
    *     path="/api/v1/workers/{id}",
    *     summary="Mostrar un usuario en función a su id",
    *     tags={"Trabajadores},
    *     description="Retorna un trabajador identificandolo por su id.",
     *    operationId="readTrabajador,
     *    @OA\Parameter(
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Operación exitosa",
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="El id ingresado es invalido"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Cliente no encontrado"
     *     ),
     * )
    */
   //  use T_Persona;
    public function readTrabajador($id){
       $dato2=Trabajador::find($id);
       $dato1=$dato2->Persona;
       //MOVER A UN MODELO
       $dato3=[
        'id'=>$dato2->id,
        'persona_id'=>$dato1->id,
        'ci'=>$dato1->ci,
        'surname'=>$dato1->surname,
        'name'=>$dato1->name,
        'address'=>$dato1->address,
        'email'=>$dato1->email,
        'fono'=>$dato1->fono,
        'characteristics'=>$dato1->characteristics,
        'characteristics 2'=>$dato2->characteristics
        ];
       return json_encode($dato3);
    } 

    public function updateTrabajador($id,Request $request){
        $this->updatePersona(Trabajador::find($id)->persona->id,$request);
        $trabajador=Trabajador::find($id);
        if($request->get('horario_entrada')!=null && $request->get('horario_entrada')!='')
            $trabajador->horario_entrada = $request->get('horario_entrada');
        if($request->get('horario_salida')!=null && $request->get('horario_salida')!='')
            $trabajador->horario_salida = $request->get('horario_salida');
        $trabajador->save();
    }

    public function deleteTrabajador(Request $request){
    	echo "borrar trabajador"; die();
    }
    public function imageTrabajador(Request $request){
    	echo "imagen trabajador"; die();
    }
    public function publicaciones($id){
        $trabajador=Trabajador::find($id)->categorias;
        //return $trabajador;
        $dato;
        foreach ($trabajador as $trabajado){
          //  echo $trabajado->id."\n";
            $publicacion = categorias_trabajador::find($trabajado->id)->publicaciones;
           // $pa=json_encode($publicacion)->ob_get_length;
          
           // echo "\ntam\n".$pa."\n";
          //  $dato['a']=json_decode($publicacion);
            echo "\n".$publicacion."\n";
        }
      //  echo "\n\n".json_encode($dato);
       // return $publicacion;
      /*
        $publicacion = categorias_trabajador::find($id)->publicaciones;
        return $publicacion;*/
    }
   /* public function publicaciones($id){
        $trabajador = Trabajador::find($id);
        echo "trabajdor \n".$trabajador."\n";
        $categoria = $trabajador->trabCats;
        echo "cate \n".$categoria;
        //return $categoria;
    }*/
        /**
    * @OA\Get(
    *     path="/api/v1/workers/",
    *     summary="Listar trabajadores",
    *     tags={"Trabajadores"},
    *     description="Retorna una pila de 10 trabajadores",
     *    operationId="listTrabajador",
     *     @OA\Response(
     *         response=200,
     *         description="Operación exitosa",
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Valor invalido"
     *     ),
     * )
    */
    public function listTrabajador(){
    	/*$trabajadores = Trabajador::take(10)->get();
        return $this->respondWithCollection($trabajadores, new TrabajadorTransformer);*/
        Trabajador::take(10)->get();
        $trabajadores = [];
        foreach (Trabajador::all() as $trabajador){
            $persona=$trabajador->Persona;
            $dato=[
                'id'=>$trabajador->id,
                'persona_id'=>$persona->id,
                'ci'=>$trabajador->ci,
                'surname'=>$trabajador->surname,
                'name'=>$trabajador->name,
                'address'=>$trabajador->address,
                'email'=>$trabajador->email,
                'fono'=>$trabajador->fono,
                'characteristics'=>$trabajador->characteristics,
                'characteristics 2'=>$trabajador->characteristics
            ];
            $trabajadores[]=$dato;
        }
        return json_encode($trabajadores);
        
    }
    
    
    public function login(Request $request){
        $jwtAuth = new JwtAuth();
        //Recibir POST
        $params = $request->all();
        
        $validator = Validator::make($params, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $email = $request->email;
        $password = $request->password;
        $getToken = $request->gettoken;
    
    
        $pwd = hash('sha256', $request->password);

        if(!is_null($email) && !is_null($password) && ($getToken == null || $getToken == 'false')){
            $signup = $jwtAuth->signup($email, $pwd);

        }elseif($getToken != null){
            //var_dump($getToken); die();
            $signup = $jwtAuth->signup($email, $pwd, $getToken);

        }else{
            $signup = array(
                    'status' => 'error',
                    'message' => 'Envia tus datos por post'
                );
        }

        return response()->json($signup, 200);
    }

    
    
    public function trabajosTrabajador(Request $request){
    	echo "trabajos del trabajador"; die();
    }
    /*
    public function categoriasTrabajador($id){
        $trabajador = Trabajador::find($id);
        $categoria = $trabajador->trabCats;
        if(!$trabajador){
            return response()->json(['mensaje'=>'No se encontro al trabajador']);
        }
        return $categoria;
    }*/
    public function llamadasdeatencionTrabajador(Request $request){
    	echo "llamadas de atencion trabajador"; die();
    }
}
