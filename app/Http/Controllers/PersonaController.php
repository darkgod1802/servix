<?php

namespace servix\Http\Controllers;

use Illuminate\Http\Request;
use servix\Persona;
use Validator;
 
use servix\Trabajador;

class PersonaController extends Controller
{
<<<<<<< HEAD
    public function create(Request $request){
        $params = $request->all();

        $validor = Validator::make($params, [
            'ci' => 'required',
            'name' => 'required',
            'fono' => 'required',
        ]);
        if(!is_null($request->ci) && !is_null($request->name) && !is_null($request->fono)){

            $persona = new Persona();
            $persona->ci = $request->ci;
            $persona->surname = $request->surname;
            $persona->name = $request->name;
            $persona->address = $request->address;
            $persona->fono = $request->fono;

            $isset_user = Persona::where('ci', '=', $request->ci)->count();
            if($isset_user == 0){
                $persona->save();

                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Persona registrada correctamente!!'
                );
            }else{
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Persona duplicado, no puede registrarse'
                );
            }
        }else{
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Persona no creada'
            );
        }

        return response()->json($data, 200);
    }

=======
            /**
     * @OA\Post(path="/v1/api/persona",
     *   tags={"Clientes"},
     *   summary="Crear cliente",
     *   description="Crear un cliente.",
     *   operationId="save",
     *   @OA\RequestBody(
     *       required=true,
     *       description="Persona creado",
     *   ),
     *   @OA\Response(response="default", description="successful operation")
     * )
     */
>>>>>>> fe951c6c8709b7534573bb5f42e1e11837afd739
    public function readPersona($id){
        return json_encode(Persona::find($id));
    }
    public function listPersona(){
        return Persona::take(10)->get();
    }
}
