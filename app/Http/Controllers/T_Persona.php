<?php

namespace servix\Http\Controllers;

use Illuminate\Http\Request;
use servix\Persona;
use servix\Trabajador;
trait T_Persona{
    public function createPersona(Request $request){
        $validatedData = $this->validate($request, [
            'ci' => 'required',
            'name' => 'required',
            'email' => 'required',
            'fono' => 'required',
        ]);
        $persona = new Persona;
        $persona->ci = $request->get('ci');
        $persona->surname = $request->get('surname');
        $persona->name = $request->get('name');
        $persona->address = $request->get('address');
        $persona->email = $request->get('email');
        $persona->fono = $request->get('fono');
        $persona->characteristics = $request->get('characteristics');
        $persona->save();
        return $persona;
    }
    public function updatePersona($id,Request $request){
        $persona=Persona::find($id);
        echo 'Persona a actualizar '.$persona;
        if($request->get('ci')!=null && $request->get('ci')!='')
            $persona->ci = $request->get('ci');
        if($request->get('surname')!=null && $request->get('surname')!='')
            $persona->surname = $request->get('surname');
        if($request->get('name')!=null && $request->get('name')!='')
            $persona->name = $request->get('name');
        if($request->get('address')!=null && $request->get('address')!='')
            $persona->address = $request->get('address');
        if($request->get('email')!=null && $request->get('email')!='')
            $persona->email = $request->get('email');
        if($request->get('fono')!=null && $request->get('fono')!='')
            $persona->fono = $request->get('fono');
        if($request->get('characteristics')!=null && $request->get('characteristics')!='')
            $persona->characteristics = $request->get('characteristics'); 
        $persona->save();
        return $persona;
    }
    /*
    public function trabajador($id){
        return Persona::find($id)->Trabajador;
    }
    public function responsable($id){
        return Persona::find($id)->Responsable;
    }
    public function readTrabajadorPersona($id){
        //   $dato1=Persona::find($id)->Trabajador;
            //PONER EN UN MODELO 
            $dato1=Persona::find($id);
            $dato2=Persona::find($id)->Trabajador;
           // $dato4=[$dato1,$dato2];
            $dato3=[
                'id'=>$dato2->id,
                'persona_id'=>$dato1->id,
                'ci'=>$dato1->ci,
                'surname'=>$dato1->surname,
                'name'=>$dato1->name,
                'address'=>$dato1->address,
                'email'=>$dato1->email,
                'fono'=>$dato1->fono,
                'characteristics'=>$dato1->characteristics,
                'horario_entrada'=>$dato2->horario_entrada,
                'horario_salida'=>$dato2->horario_salida
            ];
            return json_encode($dato3);
    }
    public function readClientePersona($id){
        $dato1=Persona::find($id);
        $dato2=Persona::find($id)->Cliente;
        $dato3=[
            'id'=>$dato2->id,
            'persona_id'=>$dato1->id,
            'ci'=>$dato1->ci,
            'surname'=>$dato1->surname,
            'name'=>$dato1->name,
            'address'=>$dato1->address,
            'email'=>$dato1->email,
            'fono'=>$dato1->fono,
            'characteristics'=>$dato1->characteristics,
            'characteristics 2'=>$dato2->characteristics
        ];
        return json_encode($dato3);
    }
    public function readResponsablePersona($id){
        $dato1=Persona::find($id);
        $dato2=Persona::find($id)->Responsable;
        $dato3=[
            'id'=>$dato2->id,
            'persona_id'=>$dato1->id,
            'ci'=>$dato1->ci,
            'surname'=>$dato1->surname,
            'name'=>$dato1->name,
            'address'=>$dato1->address,
            'email'=>$dato1->email,
            'fono'=>$dato1->fono,
            'characteristics'=>$dato1->characteristics
        ];
        return json_encode($dato3);
    }
    public function readPersona($id){
        return Persona::find($id);
    }*/

}