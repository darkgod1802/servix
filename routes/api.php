<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['jwt.verify'])->group(function (){
    //cliente
    Route::post('/clients','ClienteController@createCliente');
    Route::get('/clients/{id}','ClienteController@readCliente');
    Route::put('/clients','ClienteController@updateCliente');
    Route::delete('/clients/{id}','ClienteController@deleteCliente');
    Route::get('/clients','ClienteController@listCliente');
    Route::post('/clients/{id}/image','ClienteController@imageCliente');
    Route::get('/clients/{id}/solicitudes','ClienteController@solicitudCliente');
});

Route::group(['prefix' => 'v1'], function () {

    Route::post('/loginClient','ClienteAuthenticateController@authenticate');
    //categoria
    Route::post('/categorias','CategoriaController@createCategoria');
    Route::get('/categorias','CategoriaController@listCategoria');
    Route::delete('/categorias/{id}','CategoriaController@deleteCategoria');
    //Route::get('/categorias','CategoriaController@deleteCategoria');
    Route::get('/categorias/{id}','CategoriaController@readCategoria');
    Route::put('/categorias/{id}','CategoriaController@updateCategoria');

    //Route::post('/categorias/{id}/image','CategoriaController@imageCategoria');



    //Route::get('/persona/{id}/cliente','ClienteController@readCliente');

    //trabajador
    Route::post('/workers','TrabajadorController@createTrabajador');
    Route::get('/workers/{id}','TrabajadorController@readTrabajador');
    Route::put('/workers/{id}','TrabajadorController@updateTrabajador');
    Route::delete('/workers/{id}','TrabajadorController@deleteTrabajador');
    Route::post('/workers/{id}/image','TrabajadorController@imageTrabajador');
    //Route::post('/api/publicacionTrabajador','TrabajadorController@publicacionTrabajador');
    Route::get('/workers','TrabajadorController@listTrabajador');
    Route::get('/trabajadores/{id}/trabajos','TrabajadorController@trabajosTrabajador');
    Route::get('/trabajadores/{id}/reclamos','TrabajadorController@llamadasdeatencionTrabajador');
    //Route::get('/trabajadores/{id}/categoria','TrabajadorController@categoriasTrabajador');
    Route::get('/workers/{id}/categorias','TrabCatController@readTrabCatr');
    Route::post('/workers/{id}/categorias','TrabCatController@createTrabCatr');

    Route::get('/workers/{id}/publicaciones','TrabajadorController@publicaciones');
    //Route::get('/workers/{id}/publicaciones/{id}','TrabajadorController@publicacion');
    //Route::get('/persona/{id}/trabajador','TrabajadorController@readTrabajador');
    //Route::put('/persona/{id}/trabajador','TrabajadorController@updateTrabajador');


    //publicacion
    Route::post('/publications','PublicacionController@createPublicacion');
    Route::get('/publications/{id}','PublicacionController@readPublicacion');
    Route::put('/publications/{id}','PublicacionController@updatePublicacion');
    Route::delete('/publications/{id}','PublicacionController@deletePublicacion');
    Route::get('/publications','PublicacionController@listPublicacion');

    //Responsable -- Administrativo
    Route::post('/responsables','ResponsableController@createResponsable');
    Route::get('/responsables/{id}','ResponsableController@readResponsable');
    Route::put('/responsables/{id}','ResponsableController@updateResponsable');
    Route::delete('/responsables/{id}','ResponsableController@deleteResponsable');
    Route::get('/responsables','ResponsableController@listResponsable');
    Route::post('/responsableslogin','ResponsableController@login');
    //Route::put('/persona/{id}/responsable','ResponsableController@updateResponsable');
    //Route::get('/persona/{id}/responsable','ResponsableController@readResponsable');

    //solicitudRegistro
    Route::post('/registro','SolicitudRegistroController@solicitudRegistro');

    //tipoOpinion
    Route::get('/tipoOpinion/{id}','TipoOpinionController@readTipoOpinion');
    Route::get('/tipoOpinion','TipoOpinionController@listTipoOpinion');

    //chat
    Route::post('/api/createChatenvio','ChatController@createChatenvio');
    Route::get('/api/createChatrecibido','ChatController@createChatrecibido');


    //chat reclamos
    Route::post('/reclamo/envio','ChatReclamoController@createChatReclamoenvio');
    Route::get('/reclamo/recibo','ChatReclamoController@createChatReclamorecibido');

    //opinion
    Route::post('/opiniones/{id}','OpinionController@createOpinion');
    Route::get('/opiniones','OpinionController@listOpinion');
    Route::put('/opiniones/{id}','OpinionController@updateOpinion');
    
    //persona

    Route::post('/persona','PersonaController@create');
    Route::get('/persona/{id}','PersonaController@readPersona');
    Route::get('/persona','PersonaController@listPersona');


    //Route::put('/workers/{id}','TrabajadorController@updateTrabajador');
});

